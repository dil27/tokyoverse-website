<article class="article article-no-style">
    <div class="article-group" name="keybinds">
        <div class="article-title" name="keybinds">Default Keybinds</div>
        <div class="article-content" name="keybinds">
            <p class="keybind-group-title">General Keybinds</p>
            <div class="keybinds">
                <div class="keybind">
                    <div class="keybind-button">Y</div>
0
                    <div class="keybind-description">Answer Phone</div>
                </div>
                
                <div class="keybind">
                    <div class="keybind-button">X</div>
                    <div class="keybind-description">Cancel Emote</div>
                </div>

                <div class="keybind">
                    <div class="keybind-button">J</div>
                    <div class="keybind-description">Decline phone cell</div>
                </div>
                
                <div class="keybind">
                    <div class="keybind-button keybind-long">ALT</div>
                    <div class="keybind-description">Target</div>
                </div>
                
                <div class="keybind">
                    <div class="keybind-button">B</div>
                    <div class="keybind-description">Nunjuk</div>
                </div>
                
                <div class="keybind">
                    <div class="keybind-button">X</div>
                    <div class="keybind-description">Angkat Tangan</div>
                </div>
                
                <div class="keybind">
                    <div class="keybind-button">M</div>
                    <div class="keybind-description">Ophen Phone</div>
                </div>
                
                <div class="keybind">
                    <div class="keybind-button keybind-med">F5</div>
                    <div class="keybind-description">Scoreboard</div>
                </div>
                
                <div class="keybind">
                    <div class="keybind-button">B</div>
                    <div class="keybind-description">Seatbelt</div>
                </div>
            </div>

            <p class="keybind-group-title">Police Action</p>
            <div class="keybinds">
                <div class="keybind">
                    <div class="keybind-button keybind-med">F5</div>
                    <div class="keybind-description">Open Remote</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button keybind-med">ESC</div>
                    <div class="keybind-description">Close remote</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button keybind-long">Nm 8</div>
                    <div class="keybind-description">Lock/unlock front antenna</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button keybind-long">Nm 5</div>
                    <div class="keybind-description">Lock/unlock rear antenna</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button keybind-long">Nm 9</div>
                    <div class="keybind-description">Lock/unlock front plate</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button keybind-long">Nm 6</div>
                    <div class="keybind-description">Lock/unlock rear plate</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button">L</div>
                    <div class="keybind-description">Toggle keylock</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button">K</div>
                    <div class="keybind-description">MDT</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button">R</div>
                    <div class="keybind-description">GANTI SUARA SIREN</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button keybind-long">ALT </div>
                    <div class="keybind-description">MENYALAKAN SUARA SIREN</div>
                </div>
                <div class="keybind">
                    <div class="keybind-button">Q</div>
                    <div class="keybind-description">MENYALAKAN SIREN</div>
                </div>

                <div class="keybind"></div>
            </div>
        </div>
    </div>
</article>