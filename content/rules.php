<p>Hi, Adventurer! Welcome to <strong>Tokyo Verse Roleplay</strong> where adventure and camaraderie thrive in a world of
    fantasy and imagination.<br>
    To ensure a safe and enjoyable experience for all our players, we've established the following rules.</p>
<p>Please read and abide by them while exploring our enchanting realms.</p>
<br>
<ol>
    <li>
        <div class="number">I. </div><strong>Respect and Inclusivity.</strong>
        <ul>
            <li><div class="number">a.</div><strong>Respect Fellow Players</strong>: Treat all players with respect and kindness. Harassment,
                discrimination, or any form of offensive behavior will not be tolerated.</li>
            <li><div class="number">b.</div><strong>Inclusivity</strong>: Our community embraces diversity. Discrimination or hate speech based on
                race, gender, nationality, religion, or any other factor is strictly prohibited.</li>
        </ul>
    </li>
    <li>
        <div class="number">II. </div><strong>Roleplay Etiquette.</strong>
        <ul>
            <li><div class="number">a.</div><strong>Stay In-Character (IC)</strong>: Maintain the distinction between in-character (IC) and
                out-of-character (OOC) communication. Use OOC chat for non-roleplay discussions.</li>
            <li><div class="number">b.</div><strong>Realistic Roleplay</strong>: Strive for immersive roleplay experiences. Avoid power-gaming
                (forcing actions) and meta-gaming (using OOC information IC).</li>
            <li><div class="number">c.</div><strong>Character Backstories</strong>: Develop and share character backstories that fit the server's
                lore and theme.</li>
        </ul>
    </li>
    <li>
        <div class="number">III. </div><strong>Server Integrity</strong>
        <ul>
            <li><div class="number">a.</div><strong>No Cheating or Exploiting</strong>: Cheating, hacking, exploiting, or using unauthorized mods is
                strictly forbidden.</li>
            <li><div class="number">b.</div><strong>Server Disruptions</strong>: Do not engage in activities that disrupt the server's stability,
                such as DDoS attacks or excessive spam.</li>
            <li><div class="number">c</div><strong>Reporting Issues</strong>: Report any server issues or rule violations to our staff promptly.
            </li>
        </ul>
    </li>
    <li>
        <div class="number">IV. </div><strong>Community Interaction</strong>
        <ul>
            <li><div class="number">a.</div><strong>Use Common Sense</strong>: Exercise common sense and good judgment when making decisions
                in-game.</li>
            <li><div class="number">b.</div><strong>Cooperation</strong>: Collaboration and teamwork are encouraged. Help fellow players enjoy their
                time on the server.</li>
            <li><div class="number">c.</div><strong>Staff Respect</strong>: Show respect for our staff members and their decisions. Disputes or
                concerns can be raised through the appropriate channels.</li>
        </ul>
    </li>
    <li>
        <div class="number">V. </div><strong>Character Limitations</strong>
        <ul>
            <li><div class="number">a.</div><strong>Character Creation</strong>: Create characters that adhere to our server's lore and theme. Seek
                staff approval for unique character concepts.</li>
            <li><div class="number">b.</div><strong>Character Killing (CK)</strong>: CK requires mutual consent between players. Do not perform
                forced CKs without proper roleplay reasons.</li>
        </ul>
    </li>
    <li>
        <div class="number">VI. </div><strong>Roleplay Consent</strong>
        <ul>
            <li><div class="number">a.</div><strong>Consent-Based RP</strong>: Certain situations may require consent from all parties involved.
                Always respect the comfort levels and boundaries of other players.</li>
            <li><div class="number">b.</div><strong>Content Warnings</strong>: Use content warnings for sensitive or potentially triggering topics
                in roleplay.</li>
        </ul>
    </li>
    <li>
        <div class="number">VII. </div><strong>Meta-Gaming and Stream Sniping</strong>
        <ul>
            <li><div class="number">a.</div><strong>Meta-Gaming</strong>: Do not use information obtained through streams or external sources for
                in-game advantages.</li>
            <li><div class="number">b.</div><strong>Stream Sniping</strong>: Stream sniping, or targeting players due to their live streams, is
                strictly prohibited.</li>
        </ul>
    </li>
    <li>
        <div class="number">VII. </div><strong>Reporting</strong>
        <ul>
            <li><div class="number">a.</div><strong>Report Violations</strong>: If you witness a rule violation, report it to our staff with
                evidence when possible.</li>
            <li><div class="number">b.</div><strong>False Reports</strong>: Do not make false reports or misuse the reporting system.</li>
        </ul>
    </li>
    <li>
        <div class="number">IX. </div><strong>Disciplinary Action</strong>
        <ul>
            <li><div class="number">a.</div><strong>Enforcement</strong>: Our staff will enforce these rules fairly. Violations may result in
                warnings, temporary bans, or permanent bans, depending on the severity and frequency of the offense.
            </li>
        </ul>
    </li>
    <br>
    By joining <strong>Tokyo Verse Roleplay</strong>, you agree to abide by these rules and contribute to our thriving
    community. We hope you have a fantastic time in our fantastical world of adventure and imagination!
</ol>