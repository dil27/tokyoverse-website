<?php
    header("Content-Type: application/json");
    header('Access-Control-Allow-Origin: same-site');
    date_default_timezone_set('Asia/Jakarta');
    ini_set('display_errors', 0);

    
    $allowedServer = ['::1', 'localhost', '127.0.0.1', 'tokyoverse.local', 'tokyoverse.my.id', '172.70.189.120'];
    

   //  if (!in_array($_SERVER['REMOTE_ADDR'], $allowedServer)) {
   //     echo json_encode(Status(401));die();
   //  }

    include 'functions.api.php';

    if ($_SERVER['REQUEST_METHOD'] === "POST") {
        $data = json_decode(file_get_contents("php://input"));
        $cmd = $data->cmd;
        if (isset($cmd)) {
            if (function_exists($cmd)) {
                echo $cmd();
            } else {
                echo json_encode(Status(404));
            }
        }
    } else {
        echo json_encode(Status(404));
    }

    function Status($code) {
        switch ($code) {
            case 401:
                http_response_code(401);
                return array(
                    "error"=>true,
                    "status"=>401,
                    "message"=>"401 Unauthorized"
                );
                break;
            case 405:
                http_response_code(405);
                return array(
                    "error"=>true,
                    "status"=>405,
                    "message"=>"405 Method Not Allowed"
                );
                break;
            default:
                http_response_code(404);
                return array(
                    "error"=>true,
                    "status"=>404,
                    "message"=>"404 Not Found"
                );
                break;
        }
    }

    function sendResponse($data) {
        $array = array(
            "error"=>false,
            "status"=>200,
            "data"=>$data
        );
        
        return json_encode($array);
    }

?>
