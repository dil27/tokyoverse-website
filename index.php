<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/assets/css/home.css?t=<?= time(); ?>">
    <link rel="shortcut icon" href="/assets/images/logo/Logo-Kota.png" type="image/x-icon">
    <meta property="og:image" content="/assets/images/logo/Logo-Kota.png">
    <meta property="og:description" content="Enter the Digital Realms: Where Reality meets Fantasy!">
    <meta property="og:title" content="Tokyo Verse Roleplay">
    <meta name="twitter:card" content="/assets/images/logo/Logo-Kota.png">
    <meta name="twitter:title" content="Tokyo Verse Roleplay">
    <meta name="twitter:description" content="Enter the Digital Realms: Where Reality meets Fantasy!">
    <meta name="twitter:image" content="/assets/images/logo/Logo-Kota.png">
    <meta name="description" content="Enter the Digital Realms: Where Reality meets Fantasy!">
    <meta name="keywords" content="FiveM, Roleplay, Tokyo, Fantasy, GTA V">
    <meta name="author" content="ZeroRatenzy & TokyoVerse">
    <title>Tokyo Verse</title>
</head>
<body>
    <div class="background-set">
        <div class="bg-background"></div>
        <div class="bg-main"></div>
        <div class="bg-foreground"></div>
        <div class="bg-black"></div>
    </div>
    <!-- <div class="cursor-set">
        <div class="cursor-outline-1"></div>
        <div class="cursor-outline-2"></div>
        <div class="cursor-outline-3"></div>
        <div class="cursor-outline-4"></div>
        <div class="cursor-main"></div>
    </div> -->
    <div class="decoration dec-1">
        <div class="box o1"></div>
        <div class="box o75"></div>
        <div class="box o5"></div>
        <div class="box o25"></div>
    </div>
    <div class="decoration dec-2">
        <div class="box o25"></div>
        <div class="box o5"></div>
        <div class="box o75"></div>
        <div class="box o1"></div>
    </div>
    <div class="page">
        <nav class="navbar">
            <div data-section="#home" class="nav-item active">Home</div>
            <!-- <div data-section="#connect" class="nav-item">Connect</div> -->
            <div data-section="#discord" class="nav-item">Join the Realms</div>
            <div data-section="#rules" class="nav-item">Rules</div>
            <div data-section="#about" class="nav-item">About</div>
            <div data-section="#players" class="nav-item">Players</div>
            <div data-section="#help" class="nav-item">Help</div>
            <div class="burger-menu">
                <div class="bm-line"></div>
                <div class="bm-line"></div>
            </div>
        </nav>
        <div class="page-indicator">
        </div>
        <section class="section active" id="home">
            <div class="logo-tokyo"></div>
        </section>
        <section class="section" id="connect">
            <div class="connect-tokyo"></div>
        </section>
        <section class="section" id="discord">
            <div class="discord-tokyo">
                <div class="logo">
                    <svg fill="#ffffff" height="200px" width="200px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 219.376 219.376" xml:space="preserve"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path d="M127.518,0H40.63c-6.617,0-12,5.383-12,12v195.376c0,6.617,5.383,12,12,12h138.117c6.617,0,12-5.383,12-12V59.227 c0-3.204-1.248-6.217-3.514-8.484l-51.364-47.36C133.619,1.2,130.661,0,127.518,0z M175.747,204.376H43.63V15h71.768v40.236 c0,8.885,7.225,16.114,16.105,16.114h44.244V204.376z M131.503,56.35c-0.609,0-1.105-0.5-1.105-1.114v-31.58l34.968,32.693H131.503z M65.499,97.805c-5.14,0-9.321,4.182-9.321,9.321c0,5.14,4.182,9.321,9.321,9.321c5.14,0,9.321-4.182,9.321-9.321 C74.82,101.987,70.638,97.805,65.499,97.805z M82.499,99.627h79.5v15h-79.5V99.627z M65.499,127.805 c-5.14,0-9.321,4.182-9.321,9.321s4.182,9.321,9.321,9.321c5.14,0,9.321-4.182,9.321-9.321S70.638,127.805,65.499,127.805z M82.499,129.626h79.5v15h-79.5V129.626z M65.499,157.805c-5.14,0-9.321,4.182-9.321,9.321s4.182,9.321,9.321,9.321 c5.14,0,9.321-4.182,9.321-9.321S70.638,157.805,65.499,157.805z M82.499,159.626h79.5v15h-79.5V159.626z"></path> </g></svg>
                </div>
                <div class="text">Reserve Whitelist</div>
            </div>
        </section>
        <section class="section" id="rules">
            <article class="article">
                <?php include 'content/rules.php'; ?>
            </article>
        </section>
        <section class="section" id="about">
            <article class="article">
                <?php include 'content/about.php'; ?>
            </article>
        </section>
        <section class="section" id="players">
            <?php include 'content/online-players.php'; ?>
        </section>
        <section class="section" id="help">
            <?php include 'content/help.php'; ?>
        </section>
    </div>
    <script src="/assets/js/jquery-3.6.0.min.js"></script>
    <script src="/assets/js/home.min.js?t=<?= time(); ?>"></script>
</body>
</html>
