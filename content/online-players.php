<?php

$curl = curl_init();

$host = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];

curl_setopt_array($curl, array(
  CURLOPT_URL => $host . '/_api/api.php',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "cmd": "getPlayersOnline"
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$response = json_decode($response, true);
?>

<div class="box online-players">
    <div class="title">
        <div class="title-text">
            Online Players
        </div>
        <div class="refresh-button">
            Refresh list
        </div>
    </div>
    <div class="player-list">
        <?php

            usort($response, 'compareById');

            if (count($response) > 0) {
                foreach($response as $player) {
                    // Ping color
                    if ($player['ping'] < 50) {
                        $pingColor = "#008106";
                    } else {
                        if ($player['ping'] > 500) {
                            $pingColor = "#700c0c";
                        } else {
                            $pingColor = "#ffb700";
                        }
                    }
                    echo '
                        <div class="user" id="'. $player['identifiers'] .'">
                            <div class="user-icon"><div class="icon"></div></div>
                            <div class="user-id">'. $player['id'] .'</div>
                            <div class="user-name">'. $player['name'] .'</div>
                            <div class="user-ping" style="color: '. $pingColor .';">'. $player['ping'] .'ms</div>
                        </div>
                    ';
                }
            } else {
                echo '
                    <div class="user" id="false">
                        <div class="user-icon"><div class="icon"></div></div>
                        <div class="user-id"> </div>
                        <div class="user-name">No Online Players</div>
                        <div class="user-ping"></div>
                    </div>
                ';
            }
            

            function compareById($a, $b) {
                return strcmp($a['id'], $b['id']);
            }

        ?>
    </div>
</div>
