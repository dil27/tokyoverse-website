<?php

function getPlayersOnline() {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://server.tokyoverse.my.id/players.json',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    $players = json_decode($response, true);
    $onlinePlayers = [];
    foreach ($players as $player) {
        array_push($onlinePlayers, array(
            "id"=>$player['id'],
            "name"=>$player['name'],
            'identifiers'=>str_replace('steam:', '', $player['identifiers'][0]),
            'ping'=>$player['ping']
        ));
    }
    return json_encode($onlinePlayers);
}