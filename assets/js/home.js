let bgBg = $('.bg-background');
let bgMain = $('.bg-main');
let bgFg = $('.bg-foreground');
let cursorMain = $('.cursor-set');

$('#page-logo').addClass('active');

$(document).on('mousemove', function(event) {
    event = event || window.event;
    
    // Get mouse X position
    let mousePos = event.pageX;
    let screenWidth = $(document).width();
    let mousePosWithCenter = mousePos - (screenWidth / 2);

    // Moving background
    bgBg.css('transform', `scale(1.1) translateX(${mousePosWithCenter / 80}px)`);
    bgMain.css('transform', `scale(1.1) translateX(${mousePosWithCenter / 100}px)`);
    bgFg.css('transform', `scale(1.1) translateX(${-1 * (mousePosWithCenter / 35)}px)`);

    // Cursor Moving
    cursorMain.css('left', `${event.pageX}px`).css('top', `${event.pageY}px`);
});

$(document).on('mouseover', function() {
    cursorMain.css('opacity', '1').css('transform', 'scale(1) translate(-32px, -20px)');
});

$(document).on('mouseout', function() {
    cursorMain.css('opacity', '0').css('transform', 'scale(3) translate(-32px, -20px)');
});

// Fitting Logo to screen
let logoWidth = 1024;
let logoHeight = 1024;
let screenWidth = $(window).innerWidth();
let screenHeight = $(window).innerHeight();
let newLogoSize = null;

if (screenWidth >= screenHeight) {
    newLogoSize = screenHeight / 1.2
} else {
    newLogoSize = screenWidth / 1.2
}

if (newLogoSize) {
    $('.logo-tokyo').css('width', `${newLogoSize}px`).css('height', `${newLogoSize}px`).css('background-size', 'contain');
}

// Buttons Handler
$('.logo-tokyo').on('click', function() {
    window.open('fivem://connect/cfx.re/join/zzo375', '_self');
});

$('.discord-tokyo').on('click', function() {
    window.open('https://t.co/qgEZmV7FpK', '_blank');
});

// Scrolling Handler
// --> Get Menus and Generate Page Indicator
let menus = []
$('.page-indicator').append(`<div class="page-top"></div>`);
$('nav.navbar > .nav-item').each((index, elem) => {
    menus.push($(elem).data('section'));
    if ($(elem).data('section') == '#home') {
        $('.page-indicator').append(`<div data-indicator="${$(elem).data('section')}" class="page-item active"></div>`);
    } else {
        $('.page-indicator').append(`<div data-indicator="${$(elem).data('section')}" class="page-item"></div>`);
    }
});
$('.page-indicator').append(`<div class="page-bottom"></div>`);

let currentPage = 0; //menus[0]
let lastScroll = null;
$(window).bind('mousewheel', function(e) {
    if (!$(e.target).parent().is('.page')) {
        if (currentPage == 2 || currentPage == 3 || currentPage == 4 || currentPage == 5) {
            return;
        }
    }
    let currentTime = new Date().getTime();
    if (lastScroll == null || currentTime - lastScroll >= 250) {
        lastScroll = currentTime;
        if (e.originalEvent.wheelDelta >= 0) {
            currentPage--;
            if (currentPage < 0) {
                currentPage = menus.length - 1;
            }
        } else {
            currentPage++;
            if (currentPage > menus.length - 1) {
                currentPage = 0;
            }
        }
    } else {
        return;
    }

    activateSection(menus[currentPage]);
})

// Menu Handler
$('.nav-item').on('click', function() {
    for (var i = 0; i < menus.length; i++) {
        if (menus[i] == $(this).data('section')) {
            currentPage = i;
        }
    }
    activateSection($(this).data('section'));
});

function activateSection(elem) {
    // Switch Menu
    $('.nav-item').removeClass('active');
    $(`.nav-item[data-section="${elem}"]`).addClass('active');

    // Switch Indicator
    $('.page-item').removeClass('active');
    $(`.page-item[data-indicator="${elem}"]`).addClass('active');

    // Switch Page
    $('.section').removeClass('active');
    $(elem).addClass('active');
}

// Burger Menu Handler
$('.burger-menu').on('click', function() {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $('nav.navbar').css('background', '#fff0').css('backdrop-filter', 'blur(0px)');
        $('nav.navbar > .nav-item').css('height', '0px');
    } else {
        $(this).addClass('active');
        $('nav.navbar').css('background', '#fffa').css('backdrop-filter', 'blur(2px)');
        $('nav.navbar > .nav-item').css('height', '50px');
    }
});

$('.article-title').on('click', function() {
    let articleName = $(this).attr('name');
    $('.article-content').css('height', 0);
    $(`.article-content[name="${articleName}"]`).css('height', '50vh').css('padding-top', '20px');
});

// Refresh Player List
// -- Auto
var intervalId = window.setInterval(function(){
    refreshPlayer();    
}, 10000);

// -- Manual
$('.refresh-button').on('click', function() {
    refreshPlayer();
});

function refreshPlayer() {
    $('.player-list').html(''); //clear player list
    var settings = {
        "url": "/_api/api.php",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "cmd": "getPlayersOnline"
        }),
    };

    $.ajax(settings).done(function (response) {
        if (response.length > 0) {
            response.sort(compareById);

            response.forEach(res => {
                
                if (res.ping < 50) {
                    $pingColor = "#008106";
                } else {
                    if (res.ping > 500) {
                        $pingColor = "#700c0c";
                    } else {
                        $pingColor = "#ffb700";
                    }
                }

                $('.player-list').append(`
                    <div class="user" id="${res.identifiers}'">
                        <div class="user-icon"><div class="icon"></div></div>
                        <div class="user-id">${res.id}</div>
                        <div class="user-name">${res.name}</div>
                        <div class="user-ping" style="color: ${$pingColor};">${res.ping}ms</div>
                    </div>
                `);
            })
        }
    });
}

function compareById(a, b) {
    return a.id - b.id;
}